// 1. 简述什么是发布订阅模式，如何实现发布订阅模式
//利用队列执行
class EventEmitter {
    constructor(){
        this.queue = {};
    }

    on(keyN,keyV){
        //判断key名是否存在
        if(this.queue[keyN]){
            this.queue[keyN].push(keyV);
        }else{
            this.queue[keyN] = [keyV];
        }
    }
    

    off(keyN){
        delete this.queue[keyN];
    }

    emit(keyN,...arg){
        while(this.queue[keyN].length>0){
            this.queue[keyN].shift()(arg[1],arg[2]);
        }
    }
}

let eventBus = new EventEmitter()

let fn1 = function(name, age) {
    console.log(`${name} ${age}`)
}

let fn2 = function(name, age) {
    console.log(`hello, ${name} ${age}`)
}

eventBus.on('aaa', fn1)
eventBus.on('aaa', fn2)
eventBus.emit('aaa', false, '布兰', 12)

// 2. 深浅拷贝
//浅拷贝
let obj1 = {
    name:"a",
}

let obj2 = obj1;

obj2.name = 'b';

console.log(obj1,obj2);

//深拷贝

let obj3 = {
    name:"a",
}

let obj4 = {};

Object.keys(obj3).forEach(item=>{
    obj4[item] = obj3[item];
})

obj4.name = 'b';

console.log(obj3,obj4);

// 3. 继承 es6
class Animal {
    constructor(name) {
        this.name = name
    } 
    getName() {
        return this.name
    }
}
class Dog extends Animal {
    constructor(name, age) {
        super(name)
        this.age = age
    }
}

// 以上代码改成成es5方法

//动物函数
function Animal(name){
    this.name = name;
}

Animal.prototype.getName = () =>{
    console.log(this.name);
}

function Dog(name){
    this.age = 18;

    Animal.call(this,name);
}

Dog.prototype.habit = function(){
    console.log(this.name+"今年"+this.age);
}

let dog = new Dog('狗');
let animal = new Animal();

dog.habit();

function parseParam(url) {
    // 补全代码
    url = url.substr(url.indexOf('?') + 1);
    url = url.split('&');
    console.log(url);
    let obj = {};
    
    for(let i=0 ; i<url.length ; i++){
        obj[url[i].substring(url[i].indexOf("="),-1)] = url[i].substr(url[i].indexOf("=")+1);
    }

    console.log(obj);
}

parseParam("http://www.baidu.com?name=yihang&password=123456")
//{name: 'yihang', password: 123456}