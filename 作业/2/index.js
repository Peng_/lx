// 1. 场景题： 进入页面初始同时请求10个接口，如果有一个接口失败了，其他9个接口成功。我能通过什么方法拿到所有接口的请求状态。
// 提供思路：可以修改promose.all方法，或者包装接口返回值，或者promise es7新增方
//请求1
let p1 = new Promise((resolve, reject) => {
    resolve("请求1")
})
let p2 = new Promise((resolve, reject) => {
    resolve("请求2")
})
let p3 = new Promise((resolve, reject) => {
    resolve("请求3")
})
let p4 = new Promise((resolve, reject) => {
    resolve("请求4")
})
let p5 = new Promise((resolve, reject) => {
    reject("请求5失败")
})
let p6 = new Promise((resolve, reject) => {
    resolve("请求6")
})
let p7 = new Promise((resolve, reject) => {
    resolve("请求7")
})
let p8 = new Promise((resolve, reject) => {
    resolve("请求8")
})
let p9 = new Promise((resolve, reject) => {
    resolve("请求9")
})
let p10 = new Promise((resolve, reject) => {
    resolve("请求10")
})

function getAll(arr) {
    let promiseArr = arr.map(item => {
        return new Promise((resolve, reject) => {
            item.then(res => {
                resolve(res)
            }).catch(error => {
                resolve(error)
            })
        })
    });

    return Promise.all(promiseArr).then(res => {
        console.log(res);
    }).catch(error => {
        console.log(error);
    })
}

let apiArr = [p1, p2, p3, p4, p5, p6, p7, p8, p9, p10];

getAll(apiArr);

//2. 如何实现一个图片懒加载
// 图片全部加载完成后移除事件监听；
// 加载完的图片，从 imgList 移除；

let imgList = [...document.querySelectorAll('img')]
let length = imgList.length

function getTop(e) {
    var T = e.offsetTop;
    while (e = e.offsetParent) {
        T += e.offsetTop;
    }

    return T;
}

const imgLazyLoad = function (e) {
    // 补全代码
    let scrollH = document.documentElement.scrollTop;
    let viewH = document.documentElement.clientHeight;
    for (var i = 0; i < length; i++) {
        if (scrollH + viewH > getTop(imgList[i])) {
            imgList[i].src = imgList[i].getAttribute('data-src');
        }
    }
}

document.addEventListener('scroll', imgLazyLoad)


//3. 如何实现message方法，实现api形式的组件调用
// message.js
// import {Component} from "react"
class MessageCom extends Component {
    render() {
        return <span style={{ color: this.props.color }}>
            {this.props.text}
        </span>
    }
}

export default function (obj) {
    const { type, text } = obj;

    if (type === 'success') {
        return <MessageCom
            text={text}
            color="green"
        />
    } else if (type === 'fail') {
        return <MessageCom
            text={text}
            color="red"
        />
    }
}


import message from "./message";
class App extends Component {
    render() {
        return (
            <div>
                <button onClick={() => {
                    message({
                        type: 'fail',
                        text: "接口请求失败"
                    })
                }}>失败</button>
                <button onClick={() => {
                    message({
                        type: 'success',
                        text: "接口请求成功"
                    })
                }}>成功</button>

            </div>

        )
    }
}