// 1. 实现 new 关键字

// new 运算符用来创建用户自定义的对象类型的实例或者具有构造函数的内置对象的实例。
// 实现要点：

// new 会产生一个新对象；
// 新对象需要能够访问到构造函数的属性，所以需要重新指定它的原型；
// 构造函数可能会显示返回；

function objectFactory(...arg) {
    //取出需要实例的函数
    const Fun = arg[0];

    if(typeof Fun !== 'function'){
        throw new Error("必须是一个函数");
    }
    //创建空对象并指向需要实力的函数
    const obj = Object.create(Fun.prototype);
    //改变实例对象指向
    Fun.apply(obj,arg);
    
    return obj;
    // 补全代码
};
 
function person(name, age) {
    this.name = name
    this.age = age
}
 let p = new objectFactory(person, '布兰', 12)
 console.log(p)  // { name: '布兰', age: 12 }


//  2. 简述对bind方法的理解，补全以下代码：
Function.prototype.mybind = function (context) {
    // 补全代码
    this.apply(context)
    return this;
}

let obj = {
    name:"wjq",
    say(){
        console.log("我说"+this.name+"wjq");
    },
}

let obj1 = {
    name:"wxq",
    say(){
        console.log("我说"+this.name+"wxq");
    },
}

obj1.say.mybind(obj)



// 3. 简述apply和call方法的区别，总结以下this指向的结果，以及为什么产生该结果的原因？

// 传参区别：apply可以第二个参数是所有参数的集合，call可以传多个参数
var name = 'window';
var person = {
    name: 'person',
}
var doSth = function(){
    console.log(this.name);
    return function(){
        console.log('return:', this.name);
    }
}
var Student = {
    name: '若川',
    doSth: doSth,
}

doSth(); // window 因为他调用的是doSth函数，函数this指向是window所以指向全局
Student.doSth(); //若川 因为是从Student对象中调用的this指向Student所以this.name=若川
Student.doSth.call(person); //person 因为利用call改变了函数本身的this指向
new Student.doSth.call(person);  // 报错 new实例只能用于函数或者类
// ---------------------------
var student = {
    name: '若川',
    doSth: function(){
        console.log(this.name);
        return () => {
            console.log('arrowFn:', this.name);
        }    
    }
}
var person = {
    name: 'person',
}
student.doSth().call(person); //若川 arrowFn:若川 因为函数改变指向前就已经调用了
student.doSth.call(person)(); // person arrowFn:person 因为改变完this后调用的 箭头函数继承最近父元素的this指向

// 4. 实现函数防抖功能，写上注释。写清楚哪个是真正的事件处理程序
function debounce(func, wait, immediate) {

    let timer = null;//借助闭包来实现
    //返回一个函数事件直接执行
    return function(){
        if(!immediate){
            func();
        }else{
            //如果timer为真就清空
            if(timer){
                clearTimeout(timer);   
            }

            //赋一个新的定时器没被清空就会执行
            timer = setTimeout(func,wait);
        }
    }
}

function a(){
    console.log(1);
}

window.addEventListener('scroll',debounce(a,1000,true));

// // 5. 实现函数节流功能，写上注释。写清楚哪个是真正的事件处理程序
// options.leading 来表示是否可以立即执行一次，
// opitons.trailing 表示结束调用的时候是否还要执行一次

function throttle(func, wait, options) {
    const {leading,trailing} = options;

    if(leading){
        return function(){
            func();
        }
    }else{
        let flag = true;
        // 补全代码
        return function(){
            if(!flag){
                //冷却时间
                return;
            }
            flag = false;
            setTimeout(()=>{
                func();
                //冷却完成
                flag=true;
                if(trailing){
                    func();
                }
            },wait);
        }
    }
}

function a(){
    console.log(1);
}

window.addEventListener('scroll',throttle(a,1000,{leading:false,trailing:false}));