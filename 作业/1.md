### 1. 简述一下对 promise 的理解

Promise 是异步编程的一种解决方案：从语法上讲，promise 是一个对象，从它可以获取异步操作的消息；从本意上讲，它是承诺，承诺它过一段时间会给你一个结果。promise 有三种状态： pending(等待态)，resolved(成功态)，rejected(失败态)；状态一旦改变，就不会再变。创造 promise 实例后，它会立即执行。
即使在我短短的编程生涯中，需要使用到的 promise 也是十分多的，在需要用到异步处理并且需要回调值时，但是 promise 本身并不是异步的。

### 2 手写 Promise 实现

const PENDING = 'pending' // 等待
const FULFILLED = 'fulfilled' // 成功
const REJECTED = 'rejected' // 失败

class MyPromise {
constructor(executor) {
this.status = PENDING
this.result = undefined

    this.fulfillReactions = []
    this.rejectReactions = []
    this.isHandled = false

    const that = this
    function resolve(value) {
      // if the promise is already resolved, don't do anything
      if (that.status !== PENDING) {
        return
      }
      if (that === value) {
        // can't resolve to the same Promise
        const selfResolutionError = new TypeError('Cannot resolve to self.')
        return reject(selfResolutionError)
      }
      // if is promise
      if (typeof value === 'object' && typeof value.then === 'function') {
        value.then(resolve, reject)
      } else {
        that.status = FULFILLED
        that.result = value

        triggerReactions(that, 'fulfillReactions')
      }
    }
    function reject(value) {
      // if the promise is already resolved, don't do anything
      if (that.status !== PENDING) {
        return
      }

      that.status = REJECTED
      that.result = value

      triggerReactions(that, 'rejectReactions')
      trackError(that)
    }
    try {
      executor(resolve, reject)
    } catch (error) {
      reject(error)
    }

}

then(onFulfilled, onRejected) {
const C = this.constructor

    const promiseCapability = new PromiseCapability(C)
    return performThen(this, onFulfilled, onRejected, promiseCapability)

}

catch(onRejected) {
return this.then(null, onRejected)
}

finally(onFinally) {
const C = this.constructor

    let onFulfilled, onRejected
    if (typeof onFinally === 'function') {
      onFulfilled = value => {
        const result = onFinally.apply(undefined)
        return C.resolve(result).then(() => value)
      }
      onRejected = reason => {
        const result = onFinally.apply(undefined)
        return C.resolve(result).then(() => {
          throw reason
        })
      }
    } else {
      onFulfilled = onFinally
      onRejected = onFinally
    }

    return this.then(onFulfilled, onRejected)

}

static resolve(value) {
// 如果传进来是 promise
if (value instanceof this) {
return value
}
return new MyPromise(function(resolve) {
resolve(value)
})
}

static reject(value) {
return new MyPromise(function(resolve, reject) {
reject(value)
})
}
}

function performThen(promise, onFulfilled, onRejected, resultCapability) {
function createJob(job, value) {
if (typeof job === 'function') {
return job(value)
} else {
return value
}
}

const fulfillReaction = value => {
const result = createJob(onFulfilled, value)

    if (typeof result === 'object' && typeof result.then === 'function') {
      result.then(value => {
        resultCapability.resolve(value)
      })
    } else {
      resultCapability.resolve(result)
    }

}

const rejectReaction = value => {
const result = createJob(onRejected, value)

    if (typeof onRejected === 'function') {
      resultCapability.resolve(result)
    } else {
      resultCapability.reject(result)
    }

}

switch (promise['status']) {
case 'pending':
promise.fulfillReactions.push(fulfillReaction)
promise.rejectReactions.push(rejectReaction)
break
case 'fulfilled':
triggerReaction(fulfillReaction, promise.result)
break
case 'rejected':
{
triggerReaction(rejectReaction, promise.result)
}

      break

}
promise.isHandled = true
return resultCapability.promise
}

function triggerReactions(promise, type) {
const reactions = promise[type]
promise.fulfillReactions = undefined
promise.rejectReactions = undefined
reactions.forEach(reaction => {
triggerReaction(reaction, promise.result)
})
}

function triggerReaction(reaction, result) {
setTimeout(() => {
reaction(result)
})
}

class PromiseCapability {
constructor(C) {
this.promise = new C((resolve, reject) => {
this.resolve = resolve
this.reject = reject
})
}
}

MyPromise.all = function(promises) {
const promiseCapability = new PromiseCapability(this)
const result = []
let num = promises.length
promises.forEach((promise, index) => {
result[index] = undefined
MyPromise.resolve(promise).then(value => {
result[index] = value
num -= 1
if (num === 0) {
promiseCapability.resolve(result)
}
})
}, promiseCapability.reject)

return promiseCapability.promise
}

MyPromise.race = function(promises) {
const promiseCapability = new PromiseCapability(this)
promises.forEach(promise => {
MyPromise.resolve(promise).then(
promiseCapability.resolve,
promiseCapability.reject
)
})

return promiseCapability.promise
}

let aboutToBeNotified = new Set()
function trackError(promise) {
aboutToBeNotified.add(promise)

if (aboutToBeNotified.size > 0) {
setTimeout(() => {
const list = aboutToBeNotified

      aboutToBeNotified = new Set()
      for (const p of list) {
        if (p.isHandled) {
          continue
        }
        if (!p.isHandled) {
          console.error(`Promise rejection was not caught:${p.result}`)
        }
      }
    })

}
}
module.exports = MyPromise

### 3. 简述一下 promise.all 的使用方法，补全以下代码：

    myPromise.all = (pormiseAll) => {
    const result = [];
    let promiseCount = 0;
    let arrLength = pormiseAll.length;
    return new myPromise((resolve, reject) => {
    for (let val of pormiseAll) {
    console.log(111);
    new myPromise.resolve(val).then(
    (res) => {
    console.log(res);
    promiseCount++;
    result.push(res);
    if (promiseCount === arrLength) {
    return resolve(result);
    }
    },
    (err) => {
    return reject(err);
    }
    );
    }
    });
    };

#### 4. 简述一下 promise.race 的使用方法, 补全以下代码：

    myPromise.race=(p)=>{
    return new Promise((resolve,reject)=>{
    p.forEach(item=>{
    Promise.resolve(item).then(resolve,reject)
    })
    })
    }
